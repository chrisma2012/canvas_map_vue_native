import { propsType } from "../types/geometryTypes";
import { OpearationTypeEnum, propsMap, propsTypesEnum, setIdPool, setTempPropsArray, propsPathMap } from "../stores";
import { createId, getScaleAxis } from "../tools/utils";
import eventBus from "../tools/eventBus";
import { PATH_LINE_WIDTH } from "../config/geometryConf";
import { propsTypesEnum, OpearationTypeEnum } from '../config/enum';

import pinia from '../stores/pinia'
import { useGraphStore } from '../stores/store';
const GraphStore = useGraphStore(pinia)



let startPropId: string;
let startPosX: number;
let startPosY: number;
//挂起状态
let hangUp: boolean = false;

let pathId: string = undefined;


eventBus.$on('connect-node', (data) => {
    const { operationMode } = GraphStore;
    const { e, id } = data;
    const targetProp = propsMap.get(id)
    if (targetProp.propType === propsTypesEnum.PATH_BIDIRECTIONAL ||
        targetProp.propType === propsTypesEnum.PATH_UNIDIRECTIONAL) {
        return console.warn('选中了连线')
    }

    //两节点间连线
    if (operationMode === OpearationTypeEnum.NODE_CONNECT) {
        return setConnectPoint(id)
    }
})

eventBus.$on('connection-end', () => {
    hangUp = true;
    setTempPropsArray([])
    startPropId = undefined;
    eventBus.$emit('update-first-stage')
})

eventBus.$on('node-connect-hover', ({ e }) => {
    if (startPropId === undefined || hangUp) return;
    const { offsetX, offsetY } = e;
    const [endPosX, endPosY] = getScaleAxis(offsetX, offsetY)

    const pathConf = {
        propType: propsTypesEnum.PATH_UNIDIRECTIONAL,
        propsId: pathId,
        conf: {
            startPosX,
            startPosY,
            endPosX,
            endPosY,
            strokeColor: 'lightgrey',
            lineWidth: PATH_LINE_WIDTH,
        }
    }

    setTempPropsArray([pathConf])
    eventBus.$emit('update-first-stage')
})


function setConnectPoint(propId: string) {

    //前后点击的道具一样
    if (propId === startPropId && propId !== undefined) return;
    hangUp = false;

    if (startPropId === undefined) {
        let startPropConf = propsMap.get(propId)
        startPropId = propId;
        startPosX = startPropConf.conf.x;
        startPosY = startPropConf.conf.y;
        pathId = createId();
        return
    }
    //重置为普通状态
    setTempPropsArray([])
    hangUp = true;
    const endPropConf = propsMap.get(propId)
    endPropConf.relativePathSet.add(pathId)
    endPropConf.relativeNodeSet.add(startPropId)
    propsMap.set(propId, endPropConf)

    let startPropConf = propsMap.get(startPropId)
    startPropConf.relativePathSet.add(pathId)
    startPropConf.relativeNodeSet.add(propId)
    propsMap.set(startPropId, startPropConf)

    const pathLength = Math.sqrt(
        Math.pow(Math.abs(startPropConf.conf.x - endPropConf.conf.x), 2)
        + Math.pow(Math.abs(startPropConf.conf.y - endPropConf.conf.y), 2)
    )
    const pathConf = {
        propType: propsTypesEnum.PATH_UNIDIRECTIONAL,
        relativeNodeSet: new Set([startPropId, propId]),
        propsId: pathId,
        uid: 'path-' + (propsPathMap.size + 1),
        conf: {
            startPosX,
            startPosY,
            endPosX: endPropConf.conf.x,
            endPosY: endPropConf.conf.y,
            strokeColor: 'lightgrey',
            lineWidth: PATH_LINE_WIDTH,
            pathLength,
            startNodeId: startPropId,
            endNodeId: propId,
        }
    }

    setIdPool(pathId)
    propsMap.set(pathId, pathConf as propsType)

    propsPathMap.set(pathId, pathConf)

    eventBus.$emit('update-second-stage')
    //避免结束时，弹出节点明细框
    // setTimeout(() => {
    //     setOperationMode(OpearationTypeEnum.NO_OPERATION)
    // }, 0);
    // $('.node-connect').removeClass('active')
    //重置startPropId
    startPropId = undefined;
}

