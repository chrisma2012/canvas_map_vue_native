import eventBus from '../tools/eventBus';
import { onlyDraw, zoomStage, updateStage, dragStage, drawOffScreenCanvas, clearStage, getCurrentCanvasFrame, drawSelectArea, drawText, getPropsInRectangle } from '../geoTools/geometryTools'
import { createId, getPointPostion, getPointAxis, getLengthBetweenNode, getPointId, dpr, getScaleAxis, getOrthogonalSideByThreePoint, getOtherPropsInArea } from '../tools/utils';
import {
    useMode,
    propsMap,
    propsNodeMap,
    propsPathMap,
    tempPropsArray,
    siteIdSet,
    currentTargetProp,
    dragStartPos,
    canvasData,
    lastMousePos,
    secondCanvasFrame,
    idPool,
    selectedProps,
    benchmark,
    benchmark2,
    canvasScaleConf,
    setUseMode,
    setPropsMap,
    setPropsNodeMap,
    setPropsPathMap,
    setOperationMode,
    setCurrentTargetProp,
    setLastMousePos,
    setSecondCanvasFrame,
    setIdPool,
    setSelectedProps,
    setSiteIdSet,
    setBenchMark,
    setBenchMark2,
    setCanvasScaleConf,
    setTempPropsArray,
    setDragStartPos,
    setCurMouseEvent,
    setOtherPropsInArea,
} from "../stores";

import { ElMessage } from 'element-plus'

import { circleType, propsType } from '../types/geometryTypes';

import { PATH_LINE_WIDTH } from '../config/geometryConf';

import {
    propsTypesEnum,
    ArrowEnum,
    OpearationTypeEnum,
    useThemeModeEnum,
    mouseKeyEnum,
    pathTypesEnum,
    nodeTypesEnum,
} from '../config/enum';

import pinia from '../stores/pinia'
import { useGraphStore } from '../stores/store';
const GraphStore = useGraphStore(pinia)



//鼠标左键按下
eventBus.$on('left-mouse-down', (e) => {

    const { operationChildMode, operationMode } = GraphStore;

    const targetId = getPointId(e)
    const targetPropsConf = propsMap.get(targetId)

    const [x, y] = getScaleAxis(e.offsetX, e.offsetY)
    setLastMousePos({
        x,
        y,
        realX: e.offsetX,
        realY: e.offsetY,
        id: targetId,
        targetProps: targetPropsConf
    })

    // if (operationMode === OpearationTypeEnum.NODE_CONNECT) {
    //     if (!targetId) {
    //         //被动退出节点连线模式
    //         ElMessage({
    //             message: '已自动退出节点连线模式',
    //             type: 'warning',
    //             duration: 2000
    //         })
    //         eventBus.$emit('connection-end')
    //         GraphStore.$patch({
    //             operationMode: OpearationTypeEnum.NO_OPERATION
    //         })
    //         // setOperationMode(OpearationTypeEnum.NO_OPERATION)
    //         return;
    //     }
    // }

    //点击到空白处
    if (targetId === undefined) {
        //清空右键选中的道具
        setSelectedProps([])
    }
    //注册只执行一次的操作
    if (targetId) {
        if (e.shiftKey && benchmark.benchmarkNodeId) {

            setBenchMark2({
                ...targetPropsConf,
                ...targetPropsConf.conf
            })
        }
        eventBus.$emit('excute-once', { targetId })
    } else {
        eventBus.$emit('register-once-in-canvas-drag')
    }

    //已设置基准节点，并且按下ctrl键，并且操作的节点是基准节点的相邻节点。
    if (benchmark.benchmarkNodeId && e.ctrlKey && targetPropsConf?.relativeNodeSet.has(benchmark.benchmarkNodeId)) {
        let targetPathId = ''
        benchmark.benchmarkNode.relativePathSet.forEach(item => {
            if (targetPropsConf.relativePathSet.has(item)) {
                targetPathId = item;
                return;
            }
        })
        const targetPathConf = propsMap.get(targetPathId)

        setBenchMark({
            lenBetweenNode: targetPathConf.conf.pathLength,
            benchmarkPath: targetPathConf
        })

    }
})

//鼠标左键移动
eventBus.$on('left-mouse-move', (e) => {
    if (lastMousePos === null) return;
    const { operationChildMode, operationMode } = GraphStore;

    const { x, y, id, realX, realY } = lastMousePos;
    const [newX, newY] = getScaleAxis(e.offsetX, e.offsetY)

    // if (operationMode === OpearationTypeEnum.NODE_CONNECT) {
    //     ElMessage({
    //         message: '已自动退出节点连线模式',
    //         type: 'warning',
    //         duration: 2000
    //     })
    //     GraphStore.$patch({
    //         operationMode: OpearationTypeEnum.NO_OPERATION
    //     })

    // }

    //地图编辑模式
    if (useMode === useThemeModeEnum.EDIT_MODE) {
        //如果lastMousePos存在并且id有值（意味着点击在了道具上）,并且事件e的buttons属性值大于0（代表有按下按键）
        if (lastMousePos && id !== undefined && e.buttons > 0) {
            //拖拽事件
            if (x !== newX || y !== newY) {
                //拖拽过程中只执行一次
                eventBus.$emit('only-execute-once-in-drag', { e })
                eventBus.$emit('drag', { newX, newY, targetPropId: id, e })
            }
        }
    }


    //生产、编辑环境通用逻辑
    //点击在空白处拖动，移动整个画板
    if (id === undefined) {
        //获取第二画板上的一帧
        eventBus.$emit('register-once-to-get-secondCtxFrame')
        eventBus.$emit('drag-whole-canvas-start', {
            offsetX: e.offsetX - realX, //使用真实坐标，不是缩放坐标
            offsetY: e.offsetY - realY,//使用真实坐标，不是缩放坐标
            secondCanvasImageFrame: secondCanvasFrame
        })
    }
    //标记拖拽的初始点,
    setDragStartPos(newX, newY)
})

//鼠标左键松起
eventBus.$on('left-mouse-up', (e) => {
    if (lastMousePos === null) return; //处理拖拽事件源不是canvas的情况
    const { operationMode } = GraphStore;
    const { x, y, id, targetProps } = lastMousePos;
    const [scaleX, scaleY] = getScaleAxis(e.offsetX, e.offsetY)
    //退出角度坐标辅助工具
    if (operationMode === OpearationTypeEnum.CTRL_BENCHMARK_DRAG) {
        setOperationMode(OpearationTypeEnum.NO_OPERATION)
    }

    //地图编辑模式
    if (useMode === useThemeModeEnum.EDIT_MODE) {
        setCurMouseEvent(null)
        setOtherPropsInArea([])
        if (operationMode === OpearationTypeEnum.NODE_CONNECT) {
            //  console.log('--------id',id)
            // if (!id || propsMap.get(id).propType in pathTypesEnum) {
            //     return;
            // }
        }

        //将关联的props写回propsMap
        tempPropsArray.forEach((item: propsType | any) => {
            if ('propsId' in item) {
                propsMap.set(item.propsId, item)
            }
        })
        //重置框选的节点数组
        setSelectedProps([])

        //左键点击事件
        if (x === scaleX && y === scaleY) {
            eventBus.$emit('click', { targetProps, e, id })
        }



        //将目标道具渲染回第二层
        eventBus.$emit('update-second-stage')
        //清除第一层画布的道具
        const { firstCanvasCtx, canvasHeight, canvasWidth } = canvasData;
        clearStage(firstCanvasCtx, 0, 0, canvasWidth, canvasHeight)

        //重置
        setTempPropsArray([])
        setDragStartPos(0, 0)
    }



    //地图生产模式

    //生产、编辑环境通用逻辑

    //拖动整个画板结束逻辑
    if (id === undefined && (x !== scaleX || y !== scaleY)) {
        //TODO 需要区分是点击还是拖动触发
        eventBus.$emit('drag-whole-canvas-end', {
            offsetX: scaleX - x,
            offsetY: scaleY - y
        })
    }

    //非节点连接状态并且非画板拖动状态下才重置
    if (operationMode !== OpearationTypeEnum.NODE_CONNECT
        && operationMode !== OpearationTypeEnum.CANVAS_DRAG
    ) {
        setLastMousePos(null)
    }
})

//鼠标右键按下
eventBus.$on('right-mouse-down', (e) => {
    const { operationChildMode, operationMode } = GraphStore;

    if (operationMode === OpearationTypeEnum.NODE_CONNECT) {
        //右键点击结束连线操作
        ElMessage({
            message: '已自动退出节点连线模式',
            type: 'warning',
            duration: 2000
        })
        eventBus.$emit('connection-end')
        GraphStore.$patch({
            operationMode: OpearationTypeEnum.NO_OPERATION
        })
    }

    setLastMousePos({ x: e.offsetX, y: e.offsetY, mouseKey: mouseKeyEnum.RIGHT_KEY })
})
//鼠标右键按下移动
eventBus.$on('right-mouse-move', (e) => {
    const { x, y } = lastMousePos
    drawSelectArea(x, y, Math.abs(e.offsetX - x), Math.abs(e.offsetY - y))
})

//鼠标右键松起
eventBus.$on('right-mouse-up', (e) => {
    const { x, y } = lastMousePos;
    const { offsetX, offsetY } = e;
    if (x === offsetX && y === offsetY) {
        //右键点击事件
        eventBus.$emit('right-click', e)
        return;
    }
    eventBus.$emit('clear-first-stage')
    eventBus.$emit('excute-once-in-wheel')

    //从离屏的canvas截取对应位置的imageData数据
    const captureWidth = Math.abs(offsetX - x);
    const captureHeight = Math.abs(offsetY - y)

    if (captureWidth === 0 || captureHeight === 0) return;

    const selectedProps = getPropsInRectangle(x, y, canvasData.offCanvasCtx, captureWidth, captureHeight)
    setSelectedProps(selectedProps)

    let tem = [];
    selectedProps.forEach(item => {
        const targetConf = propsMap.get(item)
        tem = Array.from(new Set([...tem, item, ...targetConf.relativePathSet]))
    })

    //注意不能在此处删除道具。否则后续会找不到对应的道具id.
    tem = tem.map(item => propsMap.get(item))

    //将框选中的道具放进第一层临时画板容器
    setTempPropsArray(tem)
    //在第一层canvas上高亮绘制。
    eventBus.$emit('update-first-stage')
    // debugger
    setLastMousePos(null)
})


eventBus.$on('click', ({ targetProps, e, id }) => {
    const { operationMode } = GraphStore;
    //如果点击的不是空白地方
    if (targetProps !== undefined) {
        const targetProp = propsMap.get(id)
        if (operationMode === OpearationTypeEnum.NODE_CONNECT) {
            eventBus.$emit('connect-node', { e, id })
        }
        if (operationMode === OpearationTypeEnum.NO_OPERATION) {
            // console.log('点击道具的配置', propsMap.get(id))
            //有可能是连线，也有肯能是节点
            setCurrentTargetProp(targetProp)
            eventBus.$emit('click-to-show-properties', targetProp)
        }
    } else {  //点击的是空白地方
        // eventBus.$emit('click-to-hide-properties')
        eventBus.$emit('hide-context-menu')
        if (operationMode === OpearationTypeEnum.CREATE_NODE) {
            eventBus.$emit('add-new-node', e)
        }
    }
})



//鼠标hover
let lastPosId = undefined;
// const mouseCoordinateDom = $('#show-mouse-coordinate');
eventBus.$on('mouse-hover', (e) => {
    const { operationChildMode, operationMode } = GraphStore;

    const [mouseX, mouseY] = getScaleAxis(e.offsetX, e.offsetY)
    // mouseCoordinateDom.text(`坐标：（${mouseX},${mouseY}）`, true)

    //两节点间连线  
    if (operationMode === OpearationTypeEnum.NODE_CONNECT) {
        return eventBus.$emit('node-connect-hover', { e })
    }

    //创建节点时，按住shift键进行垂直、水平方向的创建。
    if (operationMode == OpearationTypeEnum.CREATE_NODE) {
        if (e.shiftKey) {
            eventBus.$emit('shiftKey-create-node-assit', e)
        }
        // else if (tempPropsArray.length > 0) {
        //     setTempPropsArray([])
        //     eventBus.$emit('update-first-stage')
        // }
        return;

    }

    const targetId = getPointId(e)
    //鼠标移入目标道具区域
    if (lastPosId === undefined && targetId !== undefined) {
        lastPosId = targetId;
        eventBus.$emit('props-hover-in', { data: propsMap.get(targetId), x: e.offsetX, y: e.offsetY })
    }

    // //鼠标移出目标道具区域
    // //将目标道具放回第二层舞台
    if (lastPosId !== undefined && targetId === undefined) {
        lastPosId = undefined
        eventBus.$emit('props-hover-out', { data: propsMap.get(targetId), x: e.offsetX, y: e.offsetY })
    }

})



let scaleRate = 1;
eventBus.$on('mouse-wheel', (e) => {
    eventBus.$emit('only-execute-once-in-wheel')
    eventBus.$emit('click-to-hide-properties')

    if (e.deltaY) {
        scaleRate += e.deltaY < 0 ? 0.1 : -0.1;
    } else if (e.detail) {
        scaleRate += e.detail > 0 ? -0.1 : 0.1;
    }
    scaleRate = scaleRate > 2 ? 2 : scaleRate < 0.1 ? 0.1 : scaleRate;
    document.querySelector(".canvas-scale span").text(`${(0.5 / scaleRate).toFixed(2)}米`, true)
    const translateX = -e.offsetX * (scaleRate - 1)
    const translateY = -e.offsetY * (scaleRate - 1)
    setCanvasScaleConf({
        translateX,
        translateY,
        scaleRate,
        scaleOriginX: e.offsetX,
        scaleOriginY: e.offsetY
    })
    eventBus.$emit('scale-stage', { translateX, translateY, scaleRate })
})


//道具拖拽
eventBus.$on('drag', (data) => {
    let { newX, newY, targetPropId, e } = data;
    getOtherPropsInArea(e)
    setCurMouseEvent(e)
    //拖动的鼠标右键框选中的道具
    if (selectedProps.indexOf(targetPropId) > -1) {
        const { x, y } = dragStartPos;
        let curTargetCircleId = '';
        //所有框选节点的位置
        tempPropsArray.forEach(item => {
            if ('x' in item.conf) {
                curTargetCircleId = item.propsId;
                if (x !== 0) {
                    item.conf.x += (newX - x);
                    item.conf.y += (newY - y);
                }
            }
            if ('endPosX' in item.conf) {
                if (x !== 0) {
                    //如果当前路线的起始节点没有被框选
                    if (selectedProps.indexOf(item.conf.startNodeId) === -1) {
                        item.conf.endPosX += (newX - x);
                        item.conf.endPosY += (newY - y);
                        return
                    }
                    //如果当前路线的结束节点没有被框选
                    if (selectedProps.indexOf(item.conf.endNodeId) === -1) {
                        item.conf.startPosX += (newX - x);
                        item.conf.startPosY += (newY - y);
                        return
                    }
                    //起始、结束节点都被框选
                    item.conf.endPosX += (newX - x);
                    item.conf.endPosY += (newY - y);
                    // } else {
                    item.conf.startPosX += (newX - x);
                    item.conf.startPosY += (newY - y);
                    // }
                }
            }
        })
    } else {
        //拖动单一节点
        let curTargetCircleId = '';
        const { benchmarkNodeId, benchmarkNode, lenBetweenNode } = benchmark;
        //假定第一个元素一定是节点（不是连线）
        tempPropsArray.forEach((item, index) => {
            if ('x' in item.conf) {
                curTargetCircleId = item.propsId;
                //如果ctrl键和shift键都没有按下，即便设置了基准点，也当作是普通拖拽。
                if (!e.ctrlKey && !e.shiftKey || !benchmarkNodeId) {
                    item.conf.x = newX;
                    item.conf.y = newY;
                    return;
                }
                //固定连线长度不变，变化角度
                if (benchmarkNodeId && item.relativeNodeSet.has(benchmarkNodeId)) {
                    //已设置基准节点，并且按下ctrl键，并且操作的节点是基准节点的相邻节点。
                    if (e.ctrlKey) {
                        [newX, newY] = getPointAxis(benchmarkNode.conf.x, benchmarkNode.conf.y, newX, newY, lenBetweenNode)
                        item.conf.x = newX;
                        item.conf.y = newY;
                        setOperationMode(OpearationTypeEnum.CTRL_BENCHMARK_DRAG)
                    }
                    //已设置基准节点，并且按下shift键，并且操作的节点是基准节点的相邻节点。
                    if (e.shiftKey) {
                        const targetLen = getOrthogonalSideByThreePoint(
                            benchmarkNode.conf.x,
                            benchmarkNode.conf.y,
                            benchmark2.x, //TODO 这里需要修改，因为值会不断变化
                            benchmark2.y,
                            e.offsetX,
                            e.offsetY
                        );
                        [newX, newY] = getPointAxis(benchmarkNode.conf.x, benchmarkNode.conf.y, benchmark2.x, benchmark2.y, targetLen)
                        item.conf.x = newX;
                        item.conf.y = newY;
                        setOperationMode(OpearationTypeEnum.CTRL_BENCHMARK_DRAG)
                    }

                }
            }
            if ('endPosX' in item.conf) {
                if (item.relativeNodeSet.has(curTargetCircleId) && item.relativeNodeSet.has(benchmarkNodeId)) {
                    item.conf.pathLength = parseInt(getLengthBetweenNode(benchmarkNode.conf.x, benchmarkNode.conf.y, newX, newY))
                }
                //判断是修改连线的起始点还是终止点
                if (item.conf.startNodeId !== curTargetCircleId) {
                    item.conf.endPosX = newX;
                    item.conf.endPosY = newY;
                    item.conf.pathLength = getLengthBetweenNode(item.conf.startPosX, item.conf.startPosY, newX, newY)
                } else {
                    item.conf.startPosX = newX;
                    item.conf.startPosY = newY;
                    item.conf.pathLength = getLengthBetweenNode(item.conf.endPosX, item.conf.endPosY, newX, newY)
                }
            }

        })
    }

    eventBus.$emit('update-first-stage')
})

//拖拽道具新建 
eventBus.$on('add-new-node', ({ offsetX, offsetY, shiftKey }) => {
    const { operationChildMode } = GraphStore;
    //第一个子节点
    let nextX, nextY;
    [offsetX, offsetY] = getScaleAxis(offsetX, offsetY)

    if (shiftKey && currentTargetProp !== null) {
        const { conf: { x, y } } = currentTargetProp;
        if (Math.abs(offsetX - x) > Math.abs(offsetY - y)) {
            nextX = offsetX;
            nextY = y;
        } else {
            nextX = x;
            nextY = offsetY;
        }
    } else {
        nextX = offsetX;
        nextY = offsetY;
    }
    //计算缩放后的鼠标坐标
    const id = createId();

    const geoConf = {
        propType: operationChildMode,
        propsId: id,
        // treeId,
        uid: 'node-' + (propsNodeMap.size + 1),
        relativeNodeSet: new Set(),
        relativePathSet: new Set(),
        conf: {
            x: nextX,
            y: nextY,
            fillColor: 'orange',
            strokeColor: 'orange',
            lineWidth: 2,
            radius: 8,

        }
    }
    siteIdSet.add(id)
    propsMap.set(id, geoConf)
    propsNodeMap.set(id, geoConf)
    setCurrentTargetProp(geoConf)
    eventBus.$emit('update-second-stage')
})

//插入新节点
eventBus.$on('insert-node', ({ e, targetId }) => {
    const { operationChildMode, operationMode } = GraphStore;
    const targetPath = propsMap.get(targetId);

    const { startNodeId: firstNodeId, endNodeId: secondNodeId } = targetPath.conf;

    const firstNode = propsMap.get(firstNodeId)
    const secondNode = propsMap.get(secondNodeId)

    let [x, y] = getScaleAxis(e.offsetX, e.offsetY)

    const temLen = getLengthBetweenNode(firstNode.conf.x, firstNode.conf.y, x, y)
    const [x1, y1] = getPointAxis(firstNode.conf.x, firstNode.conf.y, secondNode.conf.x, secondNode.conf.y, temLen);
    x = x1;
    y = y1;
    const newNodeId = createId();
    const newPathId = createId();

    firstNode.relativeNodeSet.delete(secondNodeId)
    firstNode.relativePathSet.delete(targetId)
    firstNode.relativeNodeSet.add(newNodeId)
    firstNode.relativePathSet.add(newPathId)

    secondNode.relativeNodeSet.delete(firstNodeId)
    secondNode.relativeNodeSet.add(newNodeId)


    //旧连线配置更新
    targetPath.relativeNodeSet.delete(firstNodeId)
    targetPath.relativeNodeSet.add(newNodeId)
    targetPath.conf.startNodeId = newNodeId;
    targetPath.conf.startPosX = x;
    targetPath.conf.startPosY = y;
    targetPath.conf.pathLength = getLengthBetweenNode(x, y, secondNode.conf.x, secondNode.conf.y);

    const newPathConf = {
        propType: propsTypesEnum.PATH_UNIDIRECTIONAL,
        propsId: newPathId,
        uid: 'path-' + (propsPathMap.size + 1),
        new: true,
        relativeNodeSet: new Set([firstNodeId, newNodeId]),
        conf: {
            startPosX: firstNode.conf.x,
            startPosY: firstNode.conf.y,
            endPosX: x,
            endPosY: y,
            strokeColor: 'lightgrey',
            lineWidth: PATH_LINE_WIDTH,
            pathLength: getLengthBetweenNode(firstNode.conf.x, firstNode.conf.y, x, y),
            startNodeId: firstNodeId,
            endNodeId: newNodeId
        }
    }


    //计算缩放后的鼠标坐标
    const newNodeConf = {
        propType: propsTypesEnum.COMMON_SITE,
        propsId: newNodeId,
        uid: 'node-' + (propsNodeMap.size + 1),
        relativeNodeSet: new Set([firstNodeId, secondNodeId]),
        relativePathSet: new Set([newPathId, targetId]),
        conf: {
            x,
            y,
            fillColor: 'orange',
            strokeColor: 'orange',
            lineWidth: 2,
            radius: 8,
        }
    }

    siteIdSet.add(newNodeId)
    propsMap.set(newNodeId, newNodeConf)
    propsMap.set(newPathId, newPathConf)
    propsMap.set(targetId, targetPath)
    propsMap.set(firstNodeId, firstNode)
    propsMap.set(secondNodeId, secondNode)

    propsNodeMap.set(newNodeId, newNodeConf)
    propsNodeMap.set(firstNodeId, firstNode)
    propsNodeMap.set(secondNodeId, secondNode)

    propsPathMap.set(newPathId, newPathConf)
    propsPathMap.set(targetId, targetPath)

    console.log(propsMap)

    setCurrentTargetProp(newNodeConf)
    eventBus.$emit('update-second-stage')
})


//新增子节点
eventBus.$on('node-add', ({ e, addDirection }) => {
    e.stopPropagation();
    e.preventDefault();
    if (currentTargetProp === null) return;

    const circleId = createId();
    const pathId = createId();
    //子节点配置

    const newCircleConf = {
        ...currentTargetProp,
        propsId: circleId,
        uid: 'node-' + propsNodeMap.size + 2,
        relativeNodeSet: new Set([currentTargetProp.propsId]),
        relativePathSet: new Set([pathId]),
        conf: {
            ...currentTargetProp.conf,
        }
    }
    if (addDirection === ArrowEnum.UP) {
        (newCircleConf.conf as circleType).y -= 50
    }
    if (addDirection === ArrowEnum.DOWN) {
        (newCircleConf.conf as circleType).y += 50
    }
    if (addDirection === ArrowEnum.RIGHT) {
        (newCircleConf.conf as circleType).x += 50
    }
    if (addDirection === ArrowEnum.LEFT) {
        (newCircleConf.conf as circleType).x -= 50
    }
    //两节点间连线配置
    const { startPosX, startPosY, endPosX, endPosY } = getPointPostion(
        (currentTargetProp.conf as circleType).x,
        (currentTargetProp.conf as circleType).y,
        (newCircleConf.conf as circleType).x,
        (newCircleConf.conf as circleType).y,
        8)

    const pathConf = {
        propType: propsTypesEnum.PATH_UNIDIRECTIONAL,
        propsId: pathId,
        uid: 'path-' + (propsPathMap.size + 1),
        relativeNodeSet: new Set([currentTargetProp.propsId, circleId]),
        conf: {
            startPosX,
            startPosY,
            endPosX,
            endPosY,
            strokeColor: 'lightgrey',
            lineWidth: PATH_LINE_WIDTH,
            pathLength: 50,
            startNodeId: currentTargetProp.propsId,
            endNodeId: newCircleConf.propsId
        }
    }


    //重设父节点数据    
    const parentConf = {
        ...currentTargetProp,
        relativeNodeSet: new Set([...currentTargetProp.relativeNodeSet, circleId]),
        relativePathSet: new Set([...currentTargetProp.relativePathSet, pathId]),
    }



    propsMap.set(currentTargetProp.propsId, parentConf)
    propsMap.set(pathId, pathConf)
    propsMap.set(circleId, newCircleConf)

    propsNodeMap.set(currentTargetProp.propsId, parentConf)
    propsNodeMap.set(circleId, newCircleConf)
    propsPathMap.set(pathId, pathConf)

    siteIdSet.add(circleId)
    setCurrentTargetProp(newCircleConf)
    eventBus.$emit('update-second-stage')
})


//删除子节点
eventBus.$on('node-delete', ({ targetId }) => {
    const targetProps = propsMap.get(targetId);
    const { propType } = targetProps;
    //删除连线
    if (propType in pathTypesEnum) {
        targetProps.relativeNodeSet.forEach(item => {
            const tem = propsMap.get(item)
            tem.relativePathSet.delete(targetId)
            propsMap.set(item, tem)
        })
        propsMap.delete(targetId)
        propsPathMap.delete(targetId)

    }
    //删除节点
    if (propType in nodeTypesEnum) {
        targetProps.relativeNodeSet.forEach(item => {
            const tem = propsMap.get(item)
            tem.relativeNodeSet.delete(targetId)
            propsMap.set(item, tem)
        })

        targetProps.relativePathSet.forEach(item => {
            const tem = propsMap.get(item);
            if (tem.conf.startNodeId === targetId) {
                const tem1 = propsMap.get(tem.conf.endNodeId)
                tem1.relativePathSet.delete(item)
                propsMap.set(tem.conf.endNodeId, tem1)
            } else {
                const tem1 = propsMap.get(tem.conf.startNodeId)
                tem1.relativePathSet.delete(item)
                propsMap.set(tem.conf.startNodeId, tem1)
            }
            propsMap.delete(item)
        })
        propsMap.delete(targetId)
        siteIdSet.delete(targetId)
        propsNodeMap.delete(targetId)
    }


    eventBus.$emit('update-second-stage')

})


//在拖拽事件中只执行一次
//TODO $on 改$once
eventBus.$on('excute-once', ({ targetId }) => {
    //先移除可能已绑定的执行事件
    eventBus.$off('only-execute-once-in-drag')
    eventBus.$once('only-execute-once-in-drag', ({ e }) => {
        //拖拽过程中隐藏弹窗、面板等
        eventBus.$emit('click-to-hide-properties')

        //如果是右键框选的话（不是拖动单一节点)
        if (selectedProps.length !== 0) {
            console.log('----------213edsafsa')
            //只能在此处删除道具
            tempPropsArray.forEach(item => {
                propsMap.delete(item.propsId)
            })
            eventBus.$emit('update-second-stage')
            return;
        }

        const targetProp = propsMap.get(targetId);
        if (targetProp.propType in pathTypesEnum) return console.warn('当前拖动的是连线')

        const targetConf = propsMap.get(targetId)
        setTempPropsArray([targetId, ...targetConf.relativePathSet].map(subItem => {
            const propsConf = propsMap.get(subItem);
            //从第二层舞台移除目标道具
            propsMap.delete(subItem)
            return propsConf;
        }))
        eventBus.$emit('update-second-stage')
    })
})


eventBus.$on('register-once-in-canvas-drag', () => {
    eventBus.$once('register-once-to-get-secondCtxFrame', () => {
        // GraphStore.$patch({
        //     operationMode: OpearationTypeEnum.CANVAS_DRAG
        // })
        setSecondCanvasFrame(getCurrentCanvasFrame(canvasData.secondCanvasCtx))
    })
})


eventBus.$on('drag-whole-canvas-start', ({ offsetX, offsetY, secondCanvasImageFrame }) => {
    $('#secondCanvas').addStyle({ display: "none" })
    const { firstCanvasCtx } = canvasData;
    dragStage(
        firstCanvasCtx,
        offsetX,
        offsetY,
        secondCanvasImageFrame
    )
})

eventBus.$on('drag-whole-canvas-end', ({ offsetX, offsetY }) => {

    propsMap.forEach(item => {
        if ('x' in item.conf) {
            item.conf.x = parseInt(item.conf.x + offsetX);
            item.conf.y = parseInt(item.conf.y + offsetY);
        }
        if ('startPosX' in item.conf) {
            item.conf.startPosX = item.conf.startPosX + offsetX;
            item.conf.startPosY = item.conf.startPosY + offsetY;
            item.conf.endPosX = item.conf.endPosX + offsetX;
            item.conf.endPosY = item.conf.endPosY + offsetY;
        }
        propsMap.set(item.propsId, item)
    })
    eventBus.$emit('update-second-stage')
    $('#secondCanvas').addStyle({ display: "block" })

    // setOperationMode(OpearationTypeEnum.NO_OPERATION)
})


//TODO 收集坐标逻辑统一放此处
eventBus.$on('recollect-tree-root-posx', ({ index, x }) => {
    treeRootXArr[index].x = x;
})


eventBus.$on('update-second-stage', () => {
    const { secondCanvasCtx, offCanvasCtx, canvasWidth, canvasHeight } = canvasData;

    updateStage(secondCanvasCtx, offCanvasCtx, canvasWidth, canvasHeight)
})
eventBus.$on('update-first-stage', (data) => {
    const { firstCanvasCtx, canvasWidth, canvasHeight } = canvasData;
    onlyDraw(firstCanvasCtx, canvasWidth, canvasHeight, data)
})
eventBus.$on('scale-stage', ({ translateX, translateY, scaleRate }) => {
    const { secondCanvasCtx, offCanvasCtx, canvasWidth, canvasHeight } = canvasData;


    zoomStage(secondCanvasCtx, offCanvasCtx, canvasWidth, canvasHeight, translateX, translateY, scaleRate)
})

eventBus.$on('clear-first-stage', () => {

    const { firstCanvasCtx, canvasWidth, canvasHeight } = canvasData;
    clearStage(firstCanvasCtx, 0, 0, canvasWidth, canvasHeight)
})


eventBus.$on('adjust-path-length', ({ length, data }) => {
    const { relativeNodeSet, propsId } = data as propsType;

    const { operationChildMode, operationMode } = GraphStore;

    const setArr = [...relativeNodeSet]
    let startId, endId;

    if (benchmark.benchmarkNodeId && setArr.indexOf(benchmark.benchmarkNodeId) > -1) {
        if (benchmark.benchmarkNodeId === setArr[0]) {
            [startId, endId] = setArr;
        } else {
            [endId, startId] = setArr;
        }
        if (operationMode === OpearationTypeEnum.AJUDST_PATH_LENGTH) {
            if (selectedProps.indexOf(endId) === - 1) {
                ElMessage({
                    message: '当前操作的连线与框选中的节点或基准点没有相邻关系',
                    type: 'warning',
                    duration: 2000
                })
                return
            }
        }
    } else {
        eventBus.$emit('click-to-hide-properties')
        ElMessage('你还未设置基准节点')
        return
    }

    const { conf: { x: x1, y: y1 } } = propsMap.get(startId)
    const endPoint = propsMap.get(endId)
    const { conf: { x: x2, y: y2 } } = endPoint

    const [newX, newY] = getPointAxis(x1, y1, x2, y2, length)

    const distanceInX = newX - endPoint.conf.x;
    const distanceInY = newY - endPoint.conf.y;


    let selectedPropsNode = operationMode === OpearationTypeEnum.AJUDST_PATH_LENGTH ?
        selectedProps : [endId]
    selectedPropsNode = selectedPropsNode.map(item => {
        const temNode = propsMap.get(item);
        temNode.conf.x = parseInt(temNode.conf.x + distanceInX);
        temNode.conf.y = parseInt(temNode.conf.y + distanceInY);
        propsMap.set(item, temNode)
        return temNode;
    })



    selectedPropsNode.forEach(item => {
        item.relativePathSet.forEach(subItem => {
            const temPath = propsMap.get(subItem)
            if (temPath.conf.startNodeId === item.propsId) {
                temPath.conf.startPosX = parseInt(temPath.conf.startPosX + distanceInX);
                temPath.conf.startPosY = parseInt(temPath.conf.startPosY + distanceInY);
            } else {
                temPath.conf.endPosX = parseInt(temPath.conf.endPosX + distanceInX);
                temPath.conf.endPosY = parseInt(temPath.conf.endPosY + distanceInY);
            }
            temPath.conf.pathLength = getLengthBetweenNode(
                temPath.conf.startPosX,
                temPath.conf.startPosY,
                temPath.conf.endPosX,
                temPath.conf.endPosY,
            )
            propsMap.set(subItem, temPath)
        })
    })


    eventBus.$emit('update-second-stage')
    setTempPropsArray([])
    eventBus.$emit('update-first-stage')

})

//右击面板
// eventBus.$on('right-click-in-prop', ({ propConf, e }) => {
//     RightPanel.show(e, propConf)
// })
// eventBus.$on('hideRightClickPanel', () => {
//     RightPanel.hide();
// })


// eventBus.$on('show-edit-panel', (data) => {
//     editPanel.show(true, data)
// })


eventBus.$on('path-divide', ({ targetPathId, value }) => {

    const { relativeNodeSet } = propsMap.get(targetPathId) as propsType;
    const nodeIdArr = [...relativeNodeSet] as Array<string>;
    let startNodeConf = propsMap.get(nodeIdArr[0])
    const endNodeConf = propsMap.get(nodeIdArr[1])

    let pathLength = getLengthBetweenNode(
        startNodeConf.conf.x,
        startNodeConf.conf.y,
        endNodeConf.conf.x,
        endNodeConf.conf.y
    )
    const divideLen = pathLength / value;

    // console.log(nodeIdArr, propsMap)
    //互删关联节点
    startNodeConf.relativeNodeSet.delete(endNodeConf.propsId)
    endNodeConf.relativeNodeSet.delete(startNodeConf.propsId)
    //互删关联连线
    startNodeConf.relativePathSet.delete(targetPathId)
    endNodeConf.relativePathSet.delete(targetPathId)
    //删除propsMap中的旧连线配置，但仍然使用该连线id。

    propsMap.delete(targetPathId)
    propsPathMap.delete(targetPathId);

    for (let i = 0; i < value; i++) {
        let pathId = i === 0 ? targetPathId : createId();


        let [x1, y1] = getPointAxis(
            startNodeConf.conf.x,
            startNodeConf.conf.y,
            endNodeConf.conf.x,
            endNodeConf.conf.y,
            divideLen
        )
        x1 = +x1.toFixed(2)
        y1 = +y1.toFixed(2)
        const circleId = createId();
        let nodeConf = null;
        if (i !== value - 1) {
            nodeConf = {
                propType: startNodeConf.propType,
                propsId: circleId,
                uid: 'node-' + (propsNodeMap.size + 1),
                relativeNodeSet: new Set([startNodeConf.propsId]),
                relativePathSet: new Set([pathId]),
                conf: {
                    x: x1,
                    y: y1,
                    fillColor: 'orange',
                    strokeColor: 'orange',
                    lineWidth: 2,
                    radius: 8,

                }
            }
            siteIdSet.add(circleId)
        } else {
            nodeConf = endNodeConf
            nodeConf.relativePathSet.add(pathId)
            nodeConf.relativeNodeSet.add(startNodeConf.propsId)
        }
        const pathConf = {
            propType: propsTypesEnum.PATH_UNIDIRECTIONAL,
            propsId: pathId,
            uid: 'path-' + (propsPathMap.size + 1),
            relativeNodeSet: new Set([startNodeConf.propsId, nodeConf.propsId]),
            conf: {
                startPosX: startNodeConf.conf.x,
                startPosY: startNodeConf.conf.y,
                endPosX: nodeConf.conf.x,
                endPosY: nodeConf.conf.y,
                strokeColor: 'lightgrey',
                lineWidth: PATH_LINE_WIDTH,
                pathLength: divideLen,
                startNodeId: startNodeConf.propsId,
                endNodeId: nodeConf.propsId
            }
        }

        startNodeConf.relativePathSet.add(pathId)
        startNodeConf.relativeNodeSet.add(nodeConf.propsId)
        propsMap.set(startNodeConf.propsId, startNodeConf)

        propsMap.set(pathId, pathConf)
        propsMap.set(nodeConf.propsId, nodeConf)


        propsNodeMap.set(startNodeConf.propsId, startNodeConf)
        propsNodeMap.set(nodeConf.propsId, nodeConf)
        propsPathMap.set(pathId, pathConf)

        startNodeConf = nodeConf
    }
    eventBus.$emit('update-second-stage')

})



eventBus.$on('set-target-point-benchmark', ({ targetId }) => {

    ElMessage('设置成功')
    setBenchMark({
        benchmarkNode: propsMap.get(targetId),
        benchmarkNodeId: targetId
    })

    setCurrentTargetProp(propsMap.get(targetId))
})


eventBus.$on('shiftKey-create-node-assit', ({ offsetX, offsetY }) => {

    const { conf: { x, y } } = currentTargetProp;
    let endPosX, endPosY;
    [offsetX, offsetY] = getScaleAxis(offsetX, offsetY)
    if (Math.abs(offsetX - x) > Math.abs(offsetY - y)) {
        endPosX = offsetX;
        endPosY = y;
    } else {
        endPosX = x;
        endPosY = offsetY;
    }
    const pathConf = {
        propType: propsTypesEnum.PATH_UNIDIRECTIONAL,
        conf: {
            startPosX: x,
            startPosY: y,
            endPosX,
            endPosY,
            strokeColor: 'lightblue',
            lineWidth: PATH_LINE_WIDTH,
        }
    };
    setTempPropsArray([pathConf])
    eventBus.$emit('update-first-stage')
    const pathLength = getLengthBetweenNode(x, y, endPosX, endPosY)

    let textX, textY;
    if (x === endPosX) {
        textX = x;
        textY = (y + endPosY) / 2;
    } else {
        textY = y;
        textX = (x + endPosX) / 2
    }
    drawText(canvasData.firstCanvasCtx, `${(pathLength / 50 * 0.5 / canvasScaleConf.scaleRate).toFixed(2)}米`, textX, textY)

})


// eventBus.$on('add-to-group', (data) => {
// })

eventBus.$on('adjust-position', ({ length }) => {
    setOperationMode(OpearationTypeEnum.AJUDST_PATH_LENGTH)
    eventBus.$emit('adjust-path-length', { length, data: currentTargetProp })
})

//右键点击
eventBus.$on('right-click', (e) => {
    const targetProp = propsMap.get(getPointId(e))
    if (!targetProp) return;
    setCurrentTargetProp(targetProp)

})

eventBus.$on('excute-once-in-wheel', () => {
    //先移除可能已绑定的执行事件
    eventBus.$off('only-execute-once-in-wheel')
    eventBus.$once('only-execute-once-in-wheel', () => {
        setTempPropsArray([])
        eventBus.$emit('update-first-stage')
    })
})


eventBus.$on('props-copy', () => {
    const temSelectedProps = []
    const idMap = {}

    //排除起始、结束节点没有同时被选中的路线
    const temLocalArr = tempPropsArray.filter(item => {
        const { conf } = item;
        if ('x' in conf) return true;
        if (selectedProps.indexOf(conf.startNodeId) > -1 && selectedProps.indexOf(conf.endNodeId) > -1) return true;
        return false;
    })

    const theCopyTempPropsArr = temLocalArr.map(item => {
        const temItem = { ...item }
        const propsId = createId();
        idMap[temItem.propsId] = propsId
        temItem.propsId = propsId

        if ('x' in temItem.conf) {
            temSelectedProps.push(propsId)
            temItem.uid = 'node-' + (propsNodeMap.size + 1)
            temItem.conf = { ...item.conf, x: temItem.conf.x + 20 }
            propsNodeMap.set(propsId, temItem)
            siteIdSet.add(propsId)
        }
        if ('startPosX' in temItem.conf) {
            temItem.uid = 'path-' + (propsPathMap.size + 1)
            temItem.conf = {
                ...temItem.conf,
                startPosX: temItem.conf.startPosX + 20,
                endPosX: temItem.conf.endPosX + 20,
            }
            propsPathMap.set(propsId, temItem)
        }
        setIdPool(propsId, propsId)
        propsMap.set(propsId, temItem)
        return temItem
    })

    theCopyTempPropsArr.forEach((item, index) => {
        if ('x' in item.conf) {
            const { relativePathSet } = item;
            //路线
            let tenNewPathArr = [];
            [...relativePathSet].forEach(subItem => {
                if (idMap[subItem]) {
                    tenNewPathArr.push(idMap[subItem])
                }
            })
            item.relativePathSet = new Set(tenNewPathArr)
        }
        if ('startPosX' in item.conf) {
            item.conf.endNodeId = idMap[item.conf.endNodeId]
            item.conf.startNodeId = idMap[item.conf.startNodeId]
        }

        //节点
        let tenNewNodeArr = [];
        [...item.relativeNodeSet].forEach(subItem => {
            idMap[subItem] && tenNewNodeArr.push(idMap[subItem])
        })
        item.relativeNodeSet = new Set(tenNewNodeArr)
        propsMap.set(item.propsId, item)
    })


    setSelectedProps(temSelectedProps)
    setTempPropsArray(theCopyTempPropsArr)
    drawOffScreenCanvas(canvasData.offCanvasCtx)

    eventBus.$emit('update-first-stage')
})