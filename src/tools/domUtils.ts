window.$ = (handler, searchAll: boolean = false): NodeList | HTMLElement => {
    // if (/^#(.*)+/g.test(handler))
    return searchAll ? document.querySelectorAll(handler) : document.querySelector(handler)
}

export const sleep = (time) => {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(true)
        }, time)
    })
}

export let clickEventArr = [];
export const setClickEventArr = (e) => {
    if (Array.isArray(e)) {
        clickEventArr = e;
        return
    }
    clickEventArr = [...clickEventArr, e]
}

export function isDoubleClick() {
    return new Promise((resolve) => {
        setTimeout(() => {
            if (clickEventArr.length === 2) {
                const [e1, e2] = clickEventArr;
                clickEventArr = []
                if (e1.offsetX === e2.offsetX && e1.offsetY === e2.offsetY) {

                    return resolve(true)
                }
            }
            clickEventArr = []
            return resolve(false)
        }, 200)

    })

}



HTMLElement.prototype.attr = function (attrName: string, value?: string): HTMLElement {
    if (value === undefined) {
        return this.getAttribute(attrName)
    }
    this.setAttribute(attrName, value)
    return this;
}

HTMLElement.prototype.removeAttr = function (attrName: string): HTMLElement {
    this.removeAttribute(attrName)
    return this;
}

HTMLElement.prototype.addStyle = function (styleObj: object): HTMLElement {
    let _self = this;
    Object.keys(styleObj).forEach(key => {
        _self.style[key] = styleObj[key]
    })
    return this;
}

HTMLElement.prototype.addClass = function (className: string, replaceAll: Boolean = false): HTMLElement {
    if (replaceAll) {
        this.className = className
        return this;
    }
    let tem = this.className.split(' ')
    tem = tem.concat(className.split(' '))
    tem = [...new Set(tem)].join(' ')
    if (tem) {
        this.className = tem
    }
    return this;
}
HTMLElement.prototype.hasClass = function (className: string): Boolean {

    return this.className.indexOf(className) > -1;
}

HTMLElement.prototype.removeClass = function (className: string): HTMLElement {
    let tem = this.className.split(' ')
    let index = tem.indexOf(className)
    if (index === -1) return this;
    tem.splice(index, 1)
    this.className = tem.join(' ')
    index = null;
    tem = null;
    return this;
}

HTMLElement.prototype.text = function (text: string, replaceAll: boolean = false): HTMLElement {
    if (text === undefined) return this.innerText;
    this.innerText = replaceAll ? text : `${this.innerText} ${text}`
    return this;
}

HTMLElement.prototype.val = function () {
    if (this instanceof HTMLInputElement) {
        return this.value
    }
    return new Error('当前元素不具有value属性')
}

