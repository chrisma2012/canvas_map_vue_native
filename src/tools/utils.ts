
import { DRAG_AREA_WIDTH } from '../config/constant';
import { getPropsInRectangle } from '../geoTools/geometryTools';
import { idPool, setIdPool, canvasData, canvasScaleConf, selectedProps, lastMousePos, propsMap, tempPropsArray, setOtherPropsInArea } from '../stores';


export function idToRgba(id: string) {
  return id.split("-");
}

export function rgbaToId(rgba: [number, number, number, number]) {
  return rgba.join("-");
}



export function createId(): string {


  let id = createOnceId();

  while (idPool[id]) {
    id = createOnceId();
  }
  setIdPool(id)

  return id;
}

function createOnceId(): string {
  return Array(3)
    .fill(0)
    .map(() => Math.ceil(Math.random() * 255))
    .concat(255)
    .join("-");
}


export function hitJudge(ctx: CanvasRenderingContext2D | OffscreenCanvasRenderingContext2D, x: number, y: number): string {

  const rgba = Array.from(ctx.getImageData(x * dpr, y * dpr, 1, 1).data);
  const id = rgbaToId(rgba as [number, number, number, number]);
  return propsMap.has(id) || selectedProps.indexOf(id) > -1 ? id : undefined
}


export const dpr = window.devicePixelRatio;


export const getPointPostion = (startPosX, startPosY, endPosX, endPosY, radius) => {
  //x轴相同
  if (startPosX === endPosX) {
    return {
      startPosX,
      startPosY: startPosY > endPosY ? startPosY - radius : startPosY + radius,
      endPosX,
      endPosY: startPosY > endPosY ? endPosY - radius : endPosY + radius
    }
  }
  //y轴相同
  if (startPosY === startPosY) {
    return {
      startPosX: startPosX > endPosX ? startPosX - radius : startPosX + radius,
      startPosY,
      endPosX: startPosX > endPosX ? endPosX - radius : endPosX + radius,
      endPosY
    }
  }
  const x = Math.abs(startPosX - endPosX)
  const y = Math.abs(startPosY - endPosY)
  const xian = Math.sqrt(x * x + y * y)
  if (startPosX < endPosX) {
    startPosX = startPosX + radius * x / xian
    endPosX = endPosX - radius * x / xian
  } else {
    startPosX = startPosX - radius * x / xian
    endPosX = endPosX + radius * x / xian
  }

  if (startPosY < endPosY) {
    startPosY = startPosY + radius * y / xian
    endPosY = endPosY - radius * y / xian
  } else {
    startPosY = startPosY - radius * y / xian
    endPosY = endPosY + radius * y / xian
  }

  return {
    startPosX,
    startPosY,
    endPosX,
    endPosY
  }
}

export const getLengthBetweenNode = (x1, y1, x2, y2) => {
  const xDistance = Math.abs(x2 - x1)
  const yDistance = Math.abs(y2 - y1)
  //两点间的距离
  return Math.sqrt(Math.pow(xDistance, 2) + Math.pow(yDistance, 2))
}


export const getPointAxis = (x1, y1, x2, y2, hypotenuse) => {
  if (x1 === x2) {
    //矢量箭头向下
    if (y1 < y2) return [parseInt(x1), parseInt(y1 + hypotenuse)]
    //矢量箭头向上
    return [parseInt(x1), parseInt(y1 - hypotenuse)]
  }
  if (y1 === y2) {
    //矢量箭头向右
    if (x1 < x2) return [parseInt(x1 + hypotenuse), parseInt(y1)]
    //矢量箭头向左
    return [parseInt(x1 - hypotenuse), parseInt(y1)]
  }

  //两点间的距离
  const distanceBetweenPoint = getLengthBetweenNode(x1, y1, x2, y2)
  const newXLong = Math.abs(x2 - x1) / distanceBetweenPoint * hypotenuse
  const newYLong = Math.sqrt(Math.pow(hypotenuse, 2) - Math.pow(newXLong, 2))
  if (y1 < y2) {
    //矢量箭头向第二相象
    if (x1 < x2) {
      return [parseInt(x1 + newXLong), parseInt(y1 + newYLong)]
    }
    //矢量箭头向第四相象
    if (x1 > x2) {
      return [parseInt(x1 - newXLong), parseInt(y1 + newYLong)]
    }
  } else {
    //矢量箭头向第一相象
    if (x1 < x2) {
      return [parseInt(x1 + newXLong), parseInt(y1 - newYLong)]
    }
    //矢量箭头向第三相象
    if (x1 > x2) {
      return [parseInt(x1 - newXLong), parseInt(y1 - newYLong)]
    }
  }

}


export const getScaleAxis = (x, y) => {

  const {
    scaleOriginX,
    scaleOriginY,
    scaleRate
  } = canvasScaleConf
  let len = getLengthBetweenNode(scaleOriginX, scaleOriginY, x, y)
  return getPointAxis(scaleOriginX, scaleOriginY, x, y, len / scaleRate)
}


//根据3点坐标获取某一角的直角边长
export const getOrthogonalSideByThreePoint = (
  angelAx,
  angelAy,
  angelBx,
  angelBy,
  angelCx,
  angelCy
) => {
  const sideALen = getLengthBetweenNode(angelBx, angelBy, angelCx, angelCy)
  const sideBLen = getLengthBetweenNode(angelAx, angelAy, angelCx, angelCy)
  const sideCLen = getLengthBetweenNode(angelAx, angelAy, angelBx, angelBy)
  const cosA = (Math.pow(sideCLen, 2) + Math.pow(sideBLen, 2) - Math.pow(sideALen, 2)) / (2 * sideBLen * sideCLen)
  return cosA * sideBLen
}





export const getPointId = (e) => {
  return hitJudge(canvasData.offCanvasCtx, e.offsetX, e.offsetY)
}


/**
   * 拼接成的svg格式，动态加载到前台的页面中。
   * @param {String} DomId 控件的id
   * @param {Number} imgWidth 原图像长
   * @param {Number} imgHeight 原图像高
   * @param {number} zoom 缩放比例
   */
function svgCodeLoadInHtml(DomId, imgWidth, imgHeight, zoom) {
  var image = new Image();
  var body = document.getElementById(DomId);
  var html = window.btoa('<svg xmlns=\'http://www.w3.org/2000/svg\'>' + body.innerHTML + '</svg>')
  image.src = 'data:image/svg+xml;base64,' + html;
  var newCanvas = document.createElement('canvas');
  newCanvas.width = imgWidth;
  newCanvas.height = imgHeight;
  newCanvas.style.width = imgWidth * zoom + "px";
  newCanvas.style.height = imgHeight * zoom + "px";
  var context = newCanvas.getContext("2d");
  image.onload = function () {
    context.drawImage(image, 0, 0, imgWidth, imgHeight);
  }
  return newCanvas;
}


export const getOtherPropsInArea = (e) => {
  const propsInRectangleArr = getPropsInRectangle(
    e.offsetX - DRAG_AREA_WIDTH / 2,
    e.offsetY - DRAG_AREA_WIDTH / 2,
    canvasData.offCanvasCtx,
    DRAG_AREA_WIDTH,
    DRAG_AREA_WIDTH)
  const otherPropsInAreaArr = propsInRectangleArr.filter(item => tempPropsArray.indexOf(item) === -1).map(item => propsMap.get(item))
  // console.log('区域内其他道具', otherPropsInAreaArr)
  setOtherPropsInArea(otherPropsInAreaArr)
}
