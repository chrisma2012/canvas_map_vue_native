import { EventTypes } from '../types/geometryTypes'

class EventBus {
    private eventObject: EventTypes;

    constructor() {
        this.eventObject = {};
    }

    public $on(eventType: string, callback: Function) {
        const targetEvent = this.eventObject[eventType];

        if (targetEvent) {
            this.eventObject[eventType] = [...targetEvent, callback]
        } else {
            this.eventObject[eventType] = [callback]
        }
    }

    public $once(eventType: string, callback: Function) {
        this.$on(eventType, (param?: object) => {
            this.$off(eventType)
            callback(param);
        })

    }

    public $off(eventType: string) {
        if (this.eventObject[eventType]) {
            delete this.eventObject[eventType]
        }
    }
    public $emit(eventType: string, param?: object) {
        const targetEvent = this.eventObject[eventType];
        if (targetEvent) {
            targetEvent.forEach(item => {
                item(param)
            })
        }
    }
}
let eventBus: EventBus = null;
if (!window.eventBus) {
    eventBus = window.eventBus = new EventBus();
} else {
    eventBus = window.eventBus
}



export default eventBus;