const weakMap = new WeakMap();

export const observeTarget = (target) => {
    const handler = {
        set: function (obj, prop, value) {
            const originObj = weakMap.get(this);


            return originObj.set(obj,prop)
            console.log(obj, prop, value)

            // The default behavior to store the value
            obj[prop] = value;

            // 表示成功
            return true;
        },
        has: function (target, p) {
            const originObj = weakMap.get(this);


            return originObj.get(target)
        },
        size: function (target, key, receiver) {
            const originObj = weakMap.get(this);


            return originObj.size
            console.log('--34434---', target, key, receiver)
            // 如果调用的key存在于我们自定义的拦截器里，就用我们的拦截器
            // target = handler.hasOwnProperty(key) ? interceptors : target;
            // return Reflect.get(target, key, receiver);

        },
        get: function (target, key, receiver) {

            console.log('--22222222---', target, key, receiver)
            // 如果调用的key存在于我们自定义的拦截器里，就用我们的拦截器
            // target = handler.hasOwnProperty(key) ? interceptors : target;
            // return Reflect.get(target, key, receiver);

        },
    }
    const temProxy = new Proxy(target, {
        get: function (target, key, receiver) {
            // 如果调用的key存在于我们自定义的拦截器里，就用我们的拦截器
            console.log('--3333---', target, key, receiver)

            target = handler.hasOwnProperty(key) ? handler : target;
            return Reflect.get(target, key, receiver);
        },
    })

    weakMap.set(temProxy, target)
    return temProxy
}