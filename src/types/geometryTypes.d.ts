import { propsTypesEnum } from "../stores"

export interface propBaseType {
    propType: propsTypesEnum,
    propsId: string,
    conf: circleType | pathType,
    groupId?: string,
}

export type circleType = {
    fillColor: string,
    strokeColor: string,
    lineWidth: number,
    x: number,
    y: number,
    radius: number
}

export type pathType = {
    startPosX: number,
    startPosY: number,
    endPosX: number,
    endPosY: number,
    strokeColor: string,
    lineWidth: number,
    pathLength: number,
    startNodeId: string,
    endNodeId: string,
}

export interface propsType extends propBaseType {
    treeId?: string,
    treeIndex?: number,
    uid: number,
    relativeNodeSet?: Set<string | unknown>,
    relativePathSet?: Set<string | unknown>,
}

export interface EventTypes {
    [prop: string]: Array<Function>
}


export interface treeNodeType {
    [prop: string]: Set<string>
}

