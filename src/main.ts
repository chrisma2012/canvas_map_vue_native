import { createApp } from 'vue'
import pinia from './stores/pinia';

import './style/reset.scss';
import './style/main.scss'
import './tools/domUtils';
import 'element-plus/es/components/message/style/css'
import 'element-plus/es/components/dialog/style/css'
import 'element-plus/es/components/button/style/css'
import 'element-plus/es/components/form/style/css'
import 'element-plus/es/components/form-item/style/css'
import 'element-plus/es/components/input/style/css'
import App from './App.vue'

const app = createApp(App)
app.use(pinia)

app.mount('#app')
