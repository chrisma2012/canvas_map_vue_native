const prefix = '../assets/images/sidebar/'

// export enum propsTypesEnum {
//     COMMON_SITE = 1, //普通站点
//     CHARGE_PILE = 2, //充电桩
//     LOUNGE_AREA = 3, //休息区
//     LIFT = 4,        //升降机
//     PICK_UP_SITE = 5, //接货站
//     PATH_UNIDIRECTIONAL = 6,//单向连线
//     PATH_BIDIRECTIONAL = 7,//双向连线
// }
// export const propsImgConf = {
//     2: require(`../assets/images/props/chongdianzhuang.png`),
//     5: require(`../assets/images/props/fangzi.png`),
//     // guangbiao: require(`../assets/images/props/guangbiao.png`),
//     4: require(`../assets/images/props/shengjiangji.png`),
//     3: require(`../assets/images/props/tingchechang.png`),
//     1: require(`../assets/images/props/zhandian.png`),
// }



const imgArr = [
    new URL(`../assets/images/sidebar/zhandian.png`, import.meta.url).href,
    new URL(`../assets/images/sidebar/chongdianzhuang.png`, import.meta.url).href,
    new URL(`../assets/images/sidebar/tingchechang.png`, import.meta.url).href,
    new URL(`../assets/images/sidebar/shengjiangji.png`, import.meta.url).href,
    new URL(`../assets/images/sidebar/fangzi.png`, import.meta.url).href,
]
const propsImgConf = {}
imgArr.forEach((item, index) => {
    let image = new Image();

    image.onload = function () {
        propsImgConf[index + 1] = image
    }
    image.src = item;
})

export { propsImgConf };
