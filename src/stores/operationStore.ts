import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { operationTypeEnum,nodeTypesEnum } from '@/config/enum'


export const useCounterStore = defineStore('counter', () => {
  const count = ref(0)
  const doubleCount = computed(() => count.value * 2)
  function increment() {
    count.value++
  }

  return { count, doubleCount, increment }
})

export const useOperationStore = defineStore('operationStore', () => {
  const operationType = ref(operationTypeEnum.ZHUA_SHOU)
  const operationTypeValue = ref(nodeTypesEnum.COMMON_SITE)

  function setOperationTypeValue(val) {
    operationTypeValue.value = val
  }

  function setOperationType(val) {
    operationType.value = val
  }

  return { operationType, operationTypeValue, setOperationType, setOperationTypeValue }
})
