
import { propsType } from '../types/geometryTypes';

export enum useThemeModeEnum {
    PRODUCT_MODE = 1, //(生产模式)
    EDIT_MODE = 2,  //编辑模式
}

export enum nodeTypesEnum {
    COMMON_SITE = 1, //普通站点
    CHARGE_PILE = 2, //充电桩
    LOUNGE_AREA = 3, //休息区
    LIFT = 4,        //升降机
    PICK_UP_SITE = 5, //接货站 
}

export enum pathTypesEnum {
    PATH_UNIDIRECTIONAL = 6,//单向连线
    PATH_BIDIRECTIONAL = 7,//双向连线
}

export enum propsTypesEnum {
    COMMON_SITE = 1, //普通站点
    CHARGE_PILE = 2, //充电桩
    LOUNGE_AREA = 3, //休息区
    LIFT = 4,        //升降机
    PICK_UP_SITE = 5, //接货站 
    PATH_UNIDIRECTIONAL = 6,//单向连线
    PATH_BIDIRECTIONAL = 7,//双向连线
}

export enum ArrowEnum {
    UP = 1,
    LEFT = 2,
    RIGHT = 3,
    DOWN = 4
}

export enum OpearationTypeEnum {
    NO_OPERATION = 0,
    // HANG_UP = -1,//当前处于某一操作模式下，但还在等待操作
    CANVAS_DRAG = 2, //整个画板拖动
    NODE_DRAG = 3, //节点拖动
    CTRL_BENCHMARK_DRAG = 4, //设置基准点固定连线长度拖动
    CREATE_NODE = 5,//创建节点
    NODE_CONNECT = 6,//节点连线
    AJUDST_PATH_LENGTH = 7, //距离微调
}

export enum NodeTypeEnum {
    COMMON_SITE = 1, //普通站点
    CHARGE_PILE = 2, //充电桩
    LOUNGE_AREA = 3, //休息区
    LIFT = 4,        //升降机
    PICK_UP_SITE = 5, //接货站
}


export enum ConnectTypeEnum {
    UNIDIRECTIONAL = 1,//单向连线
    BIDIRECTIONAL = 2,//双向连线
}


export enum mouseKeyEnum {
    LEFT_KEY = 1,
    RIGHT_KEY = 2
}

//使用场景
export let useMode: useThemeModeEnum = useThemeModeEnum.EDIT_MODE;
export const setUseMode = val => {
    useMode = val;
}


//画板上的所有道具集合（包含节点、路线）
export let propsMap: Map<string, propsType> = new Map();
export const setPropsMap = (value) => {
    propsMap = value
}

//画板上的所有节点道具集合
export let propsNodeMap: Map<string, propsType> = new Map();
export const setPropsNodeMap = (value) => {
    propsNodeMap = value
}

//画板上的所有路线道具集合
export let propsPathMap: Map<string, propsType> = new Map();
export const setPropsPathMap = (value) => {
    propsPathMap = value
}



//临时画板上的道具集合，该集合单纯只是用于在临时画板上展示而存在。
export let tempPropsArray: Array<object> = [];
export function setTempPropsArray(data: Array<object>) {
    tempPropsArray = data
}

//当前的主操作模式（节点连线、）等
export let operationMode: OpearationTypeEnum = OpearationTypeEnum.NO_OPERATION;
export const setOperationMode = value => {
    operationMode = value
}

//当前的子操作模式
export let operationChildMode: propsTypesEnum = 0;
export const setOperationChildMode = value => {
    operationChildMode = value
}



//当前目标节点
export let currentTargetProp: propsType = null
export function setCurrentTargetProp(data: propsType) {
    currentTargetProp = data
}


//拖动开始前的初始坐标
export let dragStartPos = {
    x: 0,
    y: 0
}
export function setDragStartPos(x: number, y: number) {
    dragStartPos = { x, y }
}


// 画板配置参数
export let canvasData = {
    firstCanvasCtx: null,
    secondCanvasCtx: null,
    offCanvasCtx: null,
    canvasWidth: 0,
    canvasHeight: 0,
}

export const setCanvasConf = (props: string | object, value?: any) => {
    if (typeof props === 'string') {
        canvasData[props] = value
    } else {
        canvasData = { ...canvasData, ...props }
    }
}


//上一次鼠标点击的位置信息
export let lastMousePos = null
export const setLastMousePos = val => {
    lastMousePos = val;
}


//拖动canvas画板时保存的secondCtx的帧
export let secondCanvasFrame = null;
export const setSecondCanvasFrame = val => {
    secondCanvasFrame = val
}

//道具Id池
export let idPool = {};
export const setIdPool = (val: string | object) => {
    if (typeof val === 'string') {
        idPool[val] = val
    } else {
        idPool = val;
    }
}

//右键框选中的道具
export let selectedProps = []
export const setSelectedProps = (val) => {
    selectedProps = val;
}



//站点的id集合
export let siteIdSet = new Set();
export const setSiteIdSet = (val) => {
    siteIdSet = val;
}


//设置基准点
export let benchmark: object = {
    benchmarkNode: null,
    benchmarkNodeId: '',
    benchmarkPath: null,
    pathBetweenNodeId: '',
    lenBetweenNode: 0,
};
export const setBenchMark = (val: object) => {
    benchmark = { ...benchmark, ...val }
}

//设置基准点2
export let benchmark2: object = {}
export const setBenchMark2 = (val: object) => {
    benchmark2 = { ...benchmark2, ...val }
}


//设置画布缩放比例
export let canvasScaleConf = {
    translateX: 0,
    translateY: 0,
    scaleRate: 1,
    scaleOriginX: 0,
    scaleOriginY: 0
};
export const setCanvasScaleConf = val => {
    canvasScaleConf = { ...canvasScaleConf, ...val };
}


//在指定区域内的非框选中的道具
export let otherPropsInArea: Array<object> = []
export const setOtherPropsInArea = (val: Array<object>) => {
    otherPropsInArea = val
}


export let curMouseEvent = null;
export const setCurMouseEvent = (val) => {
    curMouseEvent = val;
}