import { ref, computed } from 'vue'
import { defineStore } from 'pinia'


export const useGraphStore = defineStore('counter', () => {
    const graphInstance = ref(null)
    function setMapInstance(val) {
        graphInstance.value = val
    }

    return { graphInstance, setMapInstance }
})
