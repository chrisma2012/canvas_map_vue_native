import { ref, computed, reactive } from 'vue'
import { defineStore } from 'pinia'
import { OpearationTypeEnum, propsTypesEnum, useThemeModeEnum } from '../config/enum'


export const useGraphStore = defineStore('graphStore', {
    state: () => {
        return {
            operationMode: OpearationTypeEnum.NO_OPERATION,
            operationChildMode: propsTypesEnum.COMMON_SITE,
        }
    },
    getters: {
        // getAddAge: (state) => {
        //     return state.age + 100;
        //   },
        //   getNameAndAge(): string {
        //     return this.name + this.getAddAge; // 调用其它getter
        //   },
    },
    actions: {
        // saveName(name: string) {
        //   this.name = name;
        // },
        setPropsMap(value: Map<string, object>) {
            this.propsMap = value
        },
        setPropsNodeMap(value: Map<string, object>) {
            this.propsNodeMap = value
        },
        setCanvasConf(props: string | object, value?: any) {
            if (typeof props === 'string') {
                this.canvasData[props] = value
            } else {
                this.canvasData = { ...this.canvasData, ...props }
            }
        },
        setIdPool(val: string | object) {
            if (typeof val === 'string') {
                this.idPool[val] = val
            } else {
                this.idPool = val;
            }
        },
        setOperationMode(value) {
            this.operationMode = value
        },
        setOperationChildMode(value) {
            this.canvasDataoperationChildMode = value
        },
        setCurrentTargetProp(data: propsType) {
            this.currentTargetProp = data
        },
        setSecondCanvasFrame(val) {
            this.secondCanvasFrame = val
        },
 
        setDragStartPos(x: number, y: number) {
            this.dragStartPos = { x, y }
        },
        setUseMode(val) {
            this.useMode = val;
        },
        setSelectedProps(val) {
            this.selectedProps = val;
        },
        setBenchMark(val) {
            this.benchmark = { ...this.benchmark, ...val }
        },
        setCanvasScaleConf(val) {
            this.canvasScaleConf = { ...this.canvasScaleConf, ...val };
        },
        setTempPropsArray(data: Array<object>) {
            this.tempPropsArray = data
        },
        setBenchMark2(val: object) {
            this.benchmark2 = { ...this.benchmark2, ...val }
        }




    },

})


