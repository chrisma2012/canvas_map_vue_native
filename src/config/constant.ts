import { nodeTypesEnum } from "./enum";

export const RATIO = 100 / 1 // 尺比 50px : 1米

export const DRAG_AREA_WIDTH = 200; //拖动节点时，判定区域的宽度


export const PropsTypeForSelect = [{
    value: nodeTypesEnum.COMMON_SITE,
    label: "普通站点",
}, {
    value: nodeTypesEnum.CHARGE_PILE,
    label: "充电桩",
}, {
    value: nodeTypesEnum.LOUNGE_AREA,
    label: "休息区",
}, {
    value: nodeTypesEnum.LIFT,
    label: "升降机",
}, {
    value: nodeTypesEnum.PICK_UP_SITE,
    label: "接货站",
}]


export const contextMenuDataArr = [
    {
      operType: 1,
      operName: "编辑",
    },
    {
      operType: 2,
      operName: "删除",
    },
    {
      operType: 3,
      operName: "均分",
    },
    {
      operType: 4,
      operName: "设为基准",
    },
    {
      operType: 5,
      operName: "插入节点",
    },
    {
      operType: 6,
      operName: "复制",
    },
    {
      operType: 7,
      operName: "位置调整",
    },
  ]