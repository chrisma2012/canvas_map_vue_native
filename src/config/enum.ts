export enum operationTypeEnum {
    ZHUA_SHOU = 1, //
    GUANG_BIAO = 2,
    XIN_ZENG_ZHAN_DIAN = 3,
    XIN_ZENG_LIAN_XIAN = 4,
}


export enum nodeTypesEnum {
    COMMON_SITE = 1, //普通站点
    CHARGE_PILE = 2, //充电桩
    LOUNGE_AREA = 3, //休息区
    LIFT = 4,        //升降机
    PICK_UP_SITE = 5, //接货站 
}

export enum pathTypesEnum {
    PATH_UNIDIRECTIONAL = 6,//单向连线
    PATH_BIDIRECTIONAL = 7,//双向连线
}

export enum propsTypesEnum {
    COMMON_SITE = 1, //普通站点
    CHARGE_PILE = 2, //充电桩
    LOUNGE_AREA = 3, //休息区
    LIFT = 4,        //升降机
    PICK_UP_SITE = 5, //接货站 
    PATH_UNIDIRECTIONAL = 6,//单向连线
    PATH_BIDIRECTIONAL = 7,//双向连线
}

export enum OpearationTypeEnum {
    NO_OPERATION = 0,
    // HANG_UP = -1,//当前处于某一操作模式下，但还在等待操作
    CANVAS_DRAG = 2, //整个画板拖动
    NODE_DRAG = 3, //节点拖动
    CTRL_BENCHMARK_DRAG = 4, //设置基准点固定连线长度拖动
    CREATE_NODE = 5,//创建节点
    NODE_CONNECT = 6,//节点连线
    AJUDST_PATH_LENGTH = 7, //距离微调
}


export enum useThemeModeEnum {
    PRODUCT_MODE = 1, //(生产模式)
    EDIT_MODE = 2,  //编辑模式
}

 

 

export enum ArrowEnum {
    UP = 1,
    LEFT = 2,
    RIGHT = 3,
    DOWN = 4
}

 
export enum NodeTypeEnum {
    COMMON_SITE = 1, //普通站点
    CHARGE_PILE = 2, //充电桩
    LOUNGE_AREA = 3, //休息区
    LIFT = 4,        //升降机
    PICK_UP_SITE = 5, //接货站
}


export enum ConnectTypeEnum {
    UNIDIRECTIONAL = 1,//单向连线
    BIDIRECTIONAL = 2,//双向连线
}


export enum mouseKeyEnum {
    LEFT_KEY = 1,
    RIGHT_KEY = 2
}

 