// import { storeToRefs } from 'pinia'
import { Stage } from "./stage";
import { dpr, getPointId } from '../tools/utils';
import eventBus from "../tools/eventBus";
import '../eventHandlers/canvasEvent';
import '../eventHandlers/NodeConnection'
import { propsMap,setCanvasConf } from "../stores";

export default class OperStage extends Stage {
    private secondCanvasDom: HTMLCanvasElement;
    private firstCanvas: HTMLCanvasElement;
    private firstCanvasCtx: CanvasRenderingContext2D;



    constructor(canvas: HTMLCanvasElement) {
        super(canvas);
        this.secondCanvasDom = canvas;
        this.firstCanvas = this.createOperCanvas();
        this.firstCanvasCtx = this.firstCanvas.getContext('2d');
        this.firstCanvasCtx.scale(dpr, dpr);
        setCanvasConf('firstCanvasCtx', this.firstCanvasCtx)
        this.bindCanvasEvent();
    }

    createOperCanvas() {
        const styleWidth = parseInt(this.secondCanvasDom.style.width);
        const styleHeight = parseInt(this.secondCanvasDom.style.height);
        let operCanvasDom = document.createElement('canvas')
        operCanvasDom.id = 'operCanvasDom'
        operCanvasDom.width = styleWidth * dpr;
        operCanvasDom.height = styleHeight * dpr;

        operCanvasDom.style.position = 'absolute';
        operCanvasDom.style.left = `0`;
        operCanvasDom.style.top = '0';
        operCanvasDom.style.zIndex = '1';
        operCanvasDom.style.width = `${styleWidth}px`;
        operCanvasDom.style.height = `${styleHeight}px`;
        // ($('#canvas-container') as HTMLElement).appendChild(operCanvasDom)
        document.getElementById('canvas-container')?.appendChild(operCanvasDom)
        return operCanvasDom
    }

    bindCanvasEvent() {
        //鼠标右键
        this.firstCanvas.addEventListener('contextmenu', (e) => {

            const targetId = getPointId(e)
            e.stopPropagation();
            e.preventDefault();
            if (!targetId) return eventBus.$emit('right-click-in-blank', { targetId, e });
            return eventBus.$emit('show-context-menu', { e, propConf: propsMap.get(targetId) })
        })

        this.firstCanvas.addEventListener('mousedown', (e) => {
            //鼠标右键点击
            if (e.button === 2 && e.buttons === 2) {
                return eventBus.$emit('right-mouse-down', e)
            }
            //鼠标左键点击
            if (e.button === 0 && e.buttons === 1) {
                return eventBus.$emit('left-mouse-down', e)
            }

        });

        this.firstCanvas.addEventListener('mouseup', async (e) => {
            //鼠标右键点击
            if (e.button === 2 && e.buttons === 0) {
                return eventBus.$emit('right-mouse-up', e)
            }
            //鼠标左键点击
            if (e.button === 0 && e.buttons === 0) {
                return eventBus.$emit('left-mouse-up', e)
            }
        });

        this.firstCanvas.addEventListener('mousemove', (e) => {

            //鼠标右键按下拖动
            if (e.button === 0 && e.buttons === 2) {
                return eventBus.$emit('right-mouse-move', e)
            }
            //鼠标左键按下拖动
            if (e.button === 0 && e.buttons === 1) {
                return eventBus.$emit('left-mouse-move', e)
            }
            //鼠标没有按下，单纯从表面划过
            //hover
            if (e.button === 0 && e.buttons === 0) {
                return eventBus.$emit('mouse-hover', e)
            }
        });


        this.firstCanvas.addEventListener('wheel', (e: WheelEvent) => {
            e.stopPropagation();
            e.preventDefault();
            eventBus.$emit('mouse-wheel', e)
        })

    }
}
