import { propsType } from "src/types/geometryTypes";
import { canvasData, propsMap } from "../stores";
import { drawCar } from "./geometryTools";


enum AxisDiretion {
    PLUS = 1,
    MINUS = 2
}

class MoveAxis {
    private moveDistance: number;
    private totalDistance: number;
    private xDirection: AxisDiretion;
    private yDirection: AxisDiretion;
    private startPosX: number;
    private startPosY: number;
    private endPosX: number;
    private endPosY: number;
    private xLong: number;
    private yLong: number;
    private lastStartPropId: string; //上一个路线的开始节点id
    private endPropConf: propsType;
 

    constructor() {
        this.moveDistance = 0;
        this.lastStartPropId = ''
        this.endPropConf = null;
    }



    private judgeDirection(startPosX, startPosY, endPosX, endPosY) {
        if (startPosX < endPosX) this.xDirection = AxisDiretion.PLUS;
        if (startPosX > endPosX) this.xDirection = AxisDiretion.MINUS;
        if (startPosY < endPosY) this.yDirection = AxisDiretion.PLUS;
        if (startPosY > endPosY) this.yDirection = AxisDiretion.MINUS;
    }

    private getNextPointAxis() {
        //每次移动距离为1
        this.moveDistance += 1;
        if (this.startPosX === this.endPosX) {
            return this.yDirection === AxisDiretion.PLUS ? [this.startPosX, this.startPosY++] : [this.startPosX, this.startPosY--]
        }
        if (this.startPosY === this.endPosY) {
            return this.xDirection === AxisDiretion.PLUS ? [this.startPosX++, this.startPosY] : [this.startPosX--, this.startPosY]
        }

        const xLong = this.moveDistance * this.xLong / this.totalDistance
        const yLong = this.moveDistance * this.yLong / this.totalDistance;

        return [
            this.xDirection === AxisDiretion.PLUS ? this.startPosX + xLong : this.startPosX - xLong,
            this.yDirection === AxisDiretion.PLUS ? this.startPosY + yLong : this.startPosY - yLong
        ]
    }

    public init(startPropId: string) {
        this.getMoveRoute(propsMap.get(startPropId))
    }

    public animation(degree) {
        //已走到当前路线中某一段的终点
        if (this.moveDistance >= this.totalDistance) {
            return this.getMoveRoute(this.endPropConf);
        }
        const { firstCanvasCtx, canvasWidth, canvasHeight } = canvasData
        const [x, y] = this.getNextPointAxis()
        drawCar(firstCanvasCtx, x, y, canvasWidth, canvasHeight, degree)
        window.requestAnimationFrame(() => this.animation(degree))
    }

    private getMoveRoute(startPropConf: propsType) {
        const { conf: { x: startPosX, y: startPosY }, relativeNodeSet, propsId } = startPropConf;
        const relativeNodeArr = [...relativeNodeSet]
        //默认取第一个关联节点，
        this.endPropConf = propsMap.get(relativeNodeArr[0])
        // 但如果该节点是上一个已经使用过的节点的话，递推使用第2、3、。。个
        if (relativeNodeArr[0] === this.lastStartPropId) {
            if (relativeNodeArr.length > 1) {
                this.endPropConf = propsMap.get(relativeNodeArr[1])
            } else {
                return console.warn('当前已走到尽头')
            }
        }




        this.lastStartPropId = propsId
        const { conf: { x: endPosX, y: endPosY } } = this.endPropConf;

        this.xLong = Math.abs(startPosX - endPosX)
        this.yLong = Math.abs(startPosY - endPosY)
        if (startPosX === endPosX) {
            this.totalDistance = this.yLong
        } else if (startPosY === endPosY) {
            this.totalDistance = this.xLong
        } else {
            this.totalDistance = Math.sqrt(Math.pow(this.xLong, 2) + Math.pow(this.yLong, 2))
        }

        this.startPosX = startPosX
        this.startPosY = startPosY
        this.endPosX = endPosX
        this.endPosY = endPosY
        this.moveDistance = 0;

        this.judgeDirection(startPosX, startPosY, endPosX, endPosY)

        const degree = this.getDeg(startPosX, startPosY, endPosX, endPosY)
        this.animation(degree);
    }



    getDeg(startPosX, startPosY, endPosX, endPosY) {
        //前提条件：小车车头默认朝向下；当前节点（即startPos）所在位置为坐标系原点。
        //每次取第三边为 横坐标x与startPosx相等，纵坐标与startPosY相等的点。即构建一个直角三角形
        const targetPosX = startPosX;
        const targetPosY = endPosY;
        let tanStartAngle = Math.abs(targetPosX - endPosX) / Math.abs(targetPosY - startPosY)
        let angle = Math.atan(tanStartAngle); //旋转弧度

        //判断向左还是向右旋转
        //下一节点在第一或者第四相象
        if (endPosY < startPosY) {
            //下一节点在第四相象，
            if (endPosX < startPosX) {
                angle = Math.PI - angle //因为小车车头默认初始状态车头朝下，而且是顺时针旋转
            } else {//下一节点在第一相象，
                angle = angle - Math.PI //逆时针旋转
            }
        }

        //下一节点在第二或者第三相象
        if (endPosY > startPosY) {
            //下一节点在第三相象，
            if (endPosX < startPosX) {
                angle = angle //因为小车车头默认初始状态车头朝下，而且是顺时针旋转
            } else {//下一节点在第二相象，
                angle = -angle //逆时针旋转
            }
        }
        return angle
    }


}






const CarMoveInstance = new MoveAxis();
export default CarMoveInstance