import { storeToRefs } from 'pinia'
import { dpr } from '../tools/utils';

import { setCanvasConf } from "../stores";

export class Stage {
  private canvas: HTMLCanvasElement;
  private osCanvas: OffscreenCanvas;
  public ctx: CanvasRenderingContext2D;
  public osCtx: OffscreenCanvasRenderingContext2D;


  constructor(canvas: HTMLCanvasElement) {
    // 
    canvas.width = parseInt(canvas.style.width) * dpr;
    canvas.height = parseInt(canvas.style.height) * dpr;

    this.canvas = canvas;
    this.osCanvas = new OffscreenCanvas(canvas.width, canvas.height);
    this.ctx = this.canvas.getContext('2d');
    this.osCtx = this.osCanvas.getContext('2d', {
      willReadFrequently: true,
      // alpha: false,
      // storage: 'persistent',
      // desynchronized:true
    });

    setCanvasConf({
      secondCanvasCtx: this.ctx,
      offCanvasCtx: this.osCtx,
      canvasWidth: canvas.width,
      canvasHeight: canvas.height,
    })


    this.ctx.scale(dpr, dpr);
    this.osCtx.scale(dpr, dpr);

  }



}
