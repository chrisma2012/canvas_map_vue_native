import { circleType, pathType, propsType } from '../types/geometryTypes';

import { dpr, getPointAxis, getScaleAxis, idToRgba } from '../tools/utils'

import { propsImgConf } from '../js/autoInject'
import { propsTypesEnum, OpearationTypeEnum } from '../config/enum';
import {
    operationMode,
    propsMap,
    selectedProps,
    canvasData,
    lastMousePos,
    canvasScaleConf,
    tempPropsArray,
    siteIdSet,
    idPool,
    benchmark,
    otherPropsInArea,
    curMouseEvent
} from "../stores";


export function drawSelectArea(x, y, width = 100, height = 100, alpha = 0.2) {
    const { firstCanvasCtx, canvasWidth, canvasHeight } = canvasData
    clearStage(firstCanvasCtx, 0, 0, canvasWidth, canvasHeight)
    firstCanvasCtx.save();
    firstCanvasCtx.fillStyle = '#73b3cd'
    firstCanvasCtx.strokeStyle = '#73b3cd'
    firstCanvasCtx.globalAlpha = alpha;
    firstCanvasCtx.fillRect(x, y, width, height)
    firstCanvasCtx.stroke();
    firstCanvasCtx.restore()
}


import carImg from '../assets/images/car/car.png';
import { DRAG_AREA_WIDTH } from '../config/constant';
const img = new Image();
img.src = carImg.default;
export function drawCar(ctx: CanvasRenderingContext2D, x: number, y: number, width: number, height: number, degree: number) {
    clearStage(ctx, 0, 0, width, height)
    ctx.save();
    ctx.translate(x, y)
    ctx.rotate(degree);
    ctx.translate(-x, -y)
    ctx.drawImage(img, x, y, 10, 18);
    ctx.restore()
}

export function drawNode(ctx: CanvasRenderingContext2D, propConf: any, isSelected?: boolean) {
    const { x, y, propType } = propConf;

    ctx.save();
    ctx.drawImage(propsImgConf[propType], x - 8, y - 8, 16, 16);
    // ctx.beginPath();
    // ctx.fillStyle = fillColor;
    // ctx.strokeStyle = isSelected ? 'lightblue' : strokeColor;
    // ctx.lineWidth = strokeWidth;
    // ctx.arc(x, y, radius, 0, Math.PI * 2);
    // ctx.fill();
    // ctx.stroke();
    ctx.restore();
}

export function drawOffScreenCircle(osCtx: CanvasRenderingContext2D, circleConf: circleType) {
    const { lineWidth, x, y, radius, propsId } = circleConf;
    const [r, g, b, a] = idToRgba(propsId);

    osCtx.save();
    osCtx.beginPath();
    osCtx.fillStyle = `rgba(${r}, ${g}, ${b}, ${a})`;
    osCtx.strokeStyle = `rgba(${r}, ${g}, ${b}, ${a})`;
    osCtx.lineWidth = lineWidth;
    osCtx.arc(x, y, radius, 0, Math.PI * 2);
    osCtx.fill();
    osCtx.stroke();
    osCtx.restore();
}



export function drawPath(ctx: CanvasRenderingContext2D, pathConf: any, isSelected?: boolean) {
    const { startPosX, startPosY, endPosX, endPosY, strokeColor, lineWidth, pathLength, uid } = pathConf;
    ctx.beginPath();
    ctx.moveTo(startPosX, startPosY);
    ctx.lineTo(endPosX, endPosY);
    ctx.strokeStyle = isSelected ? 'lightblue' : strokeColor;
    ctx.lineWidth = lineWidth;
    ctx.stroke();
    //按住shift键拖动节点调整线段长度的距离提示
    const [x, y] = getPointAxis(startPosX, startPosY, endPosX, endPosY, pathLength / 2)
    drawText(ctx, uid, x, y + 10)

    if (pathLength) {
        drawText(ctx, (pathLength / 50 * 0.5 / canvasScaleConf.scaleRate).toFixed(2) + '米', x, y)
    }
}


export function drawOffScreenPath(osCtx: CanvasRenderingContext2D, pathConf: pathType) {
    const { startPosX, startPosY, endPosX, endPosY, lineWidth, propsId } = pathConf;

    const [r, g, b, a] = idToRgba(propsId);
    osCtx.beginPath();
    osCtx.moveTo(startPosX, startPosY);
    osCtx.lineTo(endPosX, endPosY);
    osCtx.strokeStyle = `rgba(${r}, ${g}, ${b}, ${a})`;
    osCtx.lineWidth = lineWidth;
    osCtx.stroke();
}

export function drawText(ctx: CanvasRenderingContext2D, text: string, x: number, y: number) {
    ctx.strokeStyle = 'orange';
    ctx.font = "10px 微软雅黑"
    ctx.fillText(text, x, y)
}

//辅助坐标系
export function drawAssitCoordinate(ctx, benchmarkX, benchmarkY, axisLen, alpha = 0.3) {
    const toDrawArr = [
        [benchmarkX + axisLen + 20, benchmarkY],
        [benchmarkX, benchmarkY - axisLen - 20],
        [benchmarkX - axisLen - 20, benchmarkY],
        [benchmarkX, benchmarkY + axisLen + 20]
    ]
    toDrawArr.forEach(([endPosX, endPosY]) => {
        ctx.save();
        ctx.beginPath();
        ctx.globalAlpha = alpha;
        ctx.moveTo(benchmarkX, benchmarkY);
        ctx.lineTo(endPosX, endPosY);
        ctx.strokeStyle = `lightblue`;
        ctx.lineWidth = 1;
        ctx.stroke();
        ctx.restore();
    })
}



export function drawOffScreenCanvas(osCtx: OffscreenCanvasRenderingContext2D) {

    // clearStage(osCtx, 0, 0, canvasData.canvasWidth, canvasData.canvasHeight)
    tempPropsArray.forEach(item => {
        const { conf, propsId, propType } = item;
        switch (propType) {
            case propsTypesEnum.COMMON_SITE:
            case propsTypesEnum.CHARGE_PILE:
            case propsTypesEnum.LOUNGE_AREA:
            case propsTypesEnum.LIFT:
            case propsTypesEnum.PICK_UP_SITE:
                drawOffScreenCircle(osCtx, { ...conf, propsId })
                break;
            case propsTypesEnum.PATH_UNIDIRECTIONAL:
                drawOffScreenPath(osCtx, { ...conf, propsId })
                // drawOffScreenPath(canvasData.secondCanvasCtx, { ...conf, propsId })
                break;
        }
    })
}



export function onlyDraw(ctx: CanvasRenderingContext2D, width: number, height: number, x = 0, y = 0) {
    clearStage(ctx, x, y, width, height)
    const { translateX, translateY, scaleRate } = canvasScaleConf;
    if (scaleRate !== 1) {
        ctx.save()
        ctx.translate(translateX, translateY)
        ctx.scale(scaleRate, scaleRate)
    }
    // if (operationMode === OpearationTypeEnum.CTRL_BENCHMARK_DRAG) {
    //     const { benchmarkNode: { conf: { x, y } }, lenBetweenNode } = benchmark;

    //     drawAssitCoordinate(ctx, x, y, lenBetweenNode)
    // }
    if (curMouseEvent) {
        const [x, y] = getScaleAxis(curMouseEvent.offsetX, curMouseEvent.offsetY)
        const { scaleRate } = canvasScaleConf
        drawSelectArea(
            x - DRAG_AREA_WIDTH / 2 / scaleRate,
            y - DRAG_AREA_WIDTH / 2 / scaleRate,
            DRAG_AREA_WIDTH / scaleRate,
            DRAG_AREA_WIDTH / scaleRate,
            0.1
        )
    }

    otherPropsInArea.forEach(item => {
        console.log('------111')
        drawAssitCoordinate(ctx, item.conf.x, item.conf.y, 100, 0.9)
    })



    tempPropsArray.forEach((item: propsType) => {
        const { propType, conf, uid } = item;

        switch (propType) {
            case propsTypesEnum.COMMON_SITE:
            case propsTypesEnum.CHARGE_PILE:
            case propsTypesEnum.LOUNGE_AREA:
            case propsTypesEnum.LIFT:
            case propsTypesEnum.PICK_UP_SITE:
                drawNode(ctx, { ...conf, propType }, !!selectedProps.length);
                // drawOffScreenCircle(osCtx, { ...conf, propsId })
                drawText(ctx, `(${conf.x},${conf.y})`, conf.x, conf.y + 10)
                // propsIndex &&  drawText(ctx, `(${propsIndex[0]},${propsIndex[1]})`, conf.x - 10 , conf.y )
                break;
            case propsTypesEnum.PATH_UNIDIRECTIONAL:
            case propsTypesEnum.PATH_BIDIRECTIONAL:
                drawPath(ctx, { ...conf, propType, ...item }, !!selectedProps.length)
                // drawOffScreenPath(osCtx, { ...conf, propsId })
                break;
        }
    })

    if (scaleRate !== 1) {
        ctx.restore();
    }

}

export function getCurrentCanvasFrame(ctx: CanvasRenderingContext2D) {
    const { canvasWidth, canvasHeight } = canvasData;

    // const imageData = ctx.getImageData(0, 0, canvasWidth, canvasHeight);
    // return new ImageData(new Uint8ClampedArray(imageData.data), imageData.width, imageData.height)
    return ctx.getImageData(0, 0, canvasWidth, canvasHeight);
}

//拖动整个画板
export function dragStage(
    firstCtx: CanvasRenderingContext2D,
    offsetX: number,
    offsetY: number,
    imageData: ImageData
) {
    clearStage(firstCtx, 0, 0, imageData.width, imageData.height)
    firstCtx.save();
    firstCtx.putImageData(imageData, offsetX * dpr, offsetY * dpr)
    firstCtx.restore();
}

export function zoomStage(
    ctx: CanvasRenderingContext2D,
    osCtx: CanvasRenderingContext2D,
    width: number,
    height: number,
    translateX = 0,
    translateY = 0,
    scaleRate
) {
    clearStage(ctx, 0, 0, width, height)
    clearStage(osCtx, 0, 0, width, height)

    ctx.save()
    ctx.translate(translateX, translateY)
    ctx.scale(scaleRate, scaleRate)
    osCtx.save()
    osCtx.translate(translateX, translateY)
    osCtx.scale(scaleRate, scaleRate)

    drawProps(ctx, osCtx)


    ctx.restore();
    osCtx.restore();
}




export function updateStage(
    ctx: CanvasRenderingContext2D,
    osCtx: OffscreenCanvasRenderingContext2D | CanvasRenderingContext2D,
    width: number,
    height: number,
    x = 0,
    y = 0
) {
    clearStage(ctx, x, y, width, height)
    clearStage(osCtx, x, y, width, height)
    const { translateX, translateY, scaleRate } = canvasScaleConf;
    // const { translateX, translateY, scaleRate } = canvasScaleConf;
    if (scaleRate !== 1) {
        ctx.save()
        ctx.translate(translateX, translateY)
        ctx.scale(scaleRate, scaleRate)
        osCtx.save()
        osCtx.translate(translateX, translateY)
        osCtx.scale(scaleRate, scaleRate)
    }
    drawProps(ctx, osCtx)
    if (scaleRate !== 1) {
        ctx.restore();
        osCtx.restore();
    }
}


function drawProps(ctx: CanvasRenderingContext2D, osCtx: OffscreenCanvasRenderingContext2D) {
    for (const item of propsMap.values()) {
        const { propType, conf, propsId, uid } = item;


        switch (propType) {
            case propsTypesEnum.COMMON_SITE:
            case propsTypesEnum.CHARGE_PILE:
            case propsTypesEnum.LOUNGE_AREA:
            case propsTypesEnum.LIFT:
            case propsTypesEnum.PICK_UP_SITE:
                drawNode(ctx, { ...conf, propType });
                drawOffScreenCircle(osCtx, { ...conf, propsId })
                drawText(ctx, `(${conf.x},${conf.y})`, conf.x + 10, conf.y + 10)
                uid && drawText(ctx, `${uid}`, conf.x - 3, conf.y - 10)

                break;
            case propsTypesEnum.PATH_UNIDIRECTIONAL:

                drawPath(ctx, { ...conf, propType, uid })
                drawOffScreenPath(osCtx, { ...conf, propsId })
                break;
        }
    }
}

export function clearStage(ctx, x, y, width, height) {
    ctx.clearRect(x, y, width, height)
}



export function getPropsInRectangle(x, y, ctx, width = 200, height = 200) {
    let imagedata = ctx.getImageData(x * dpr, y * dpr, width * dpr, height * dpr)
    imagedata = imagedata.data;
    const propsIdSet = new Set([]);
    //遍历每个像素的数据，拼接成框选中的道具id。
    for (let i = 0; i < imagedata.length; i += 4) {
        if (imagedata[i] === 255 && imagedata[i + 1] === 255 && imagedata[i + 2] === 255) continue;
        if (imagedata[i] === 0 && imagedata[i + 1] === 0 && imagedata[i + 2] === 0) continue;
        let id = `${imagedata[i]}-${imagedata[i + 1]}-${imagedata[i + 2]}-255`
        if (siteIdSet.has(id)) propsIdSet.add(id)
    }
    let idPoolArr = Object.keys(idPool);
    let selectedProps = []
    //筛选有效的道具id。
    if (propsIdSet.size > idPoolArr.length) {
        selectedProps = idPoolArr.filter(item => {
            return propsIdSet.has(item)
        })
    } else {
        selectedProps = [...propsIdSet].filter(item => {
            return idPoolArr.indexOf(item) > -1
        })
    }
    return selectedProps
}